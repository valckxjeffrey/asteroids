# Instructions as a backup, in case I forget 

# Installing TypeScript

This is a skeleton app for people who want to use TypeScript. A Vagrantfile is included for those who want to run it in a virtual machine.

You don't have to use the Vagrantfile if you're fine with installing NodeJS and NPM locally. Feel free to delete it if that's the case.

## Instructions

1. Clone this repository to your pc
2. Make sure you have [NodeJS](https://nodejs.org/en/download/) installed (preferably the LTS version). This will also install `npm`.
3. Open a terminal window (command prompt, git bash, powershell)
4. Check if NodeJS is installed by typing `node --version` into the terminal. It should print a line with something like `v8.12.0`.
5. Check if NPM is installed by typing  `npm --version` into the terminal. It should print a line with something like `6.4.1`.
6. As an alternative to NPM, you could use the [Yarn](yarnpkg.com) npm client from Google and Facebook (it works slightly better and faster than NPM). Install by running `npm install -g yarn`
7. Install de dependencies by running `npm install` or `yarn install` from the project directory.
8. Compile the project by running `npm run build` or `yarn run build`. If you want to run the build script everytime you make changes automatically, you can use `npm run watch` or `yarn run watch`. To check out how this works, you can open the `package.json`.
9. Open the `tsconfig.json` file in this project.
10. Search the [TypeScript documentation](https://www.typescriptlang.org/docs/home.html) and try to explain every line in the json file. Don't understand something? That's fine! Just don't copy stuff for the sake of filling up your answers.

# This is the Repo for the class Game Development 101, where we build the game 'Asteroids'


