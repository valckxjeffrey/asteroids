class Ship {
    constructor(name, color, cannons, distanceTravelled, health) {
        this.name = name;
        this.color = color;
        this.cannons = cannons;
        this.distanceTravelled = distanceTravelled;
        this.health = health;
    }
    addDistanceTravelled() {
        this.distanceTravelled++;
    }
    calculateDamage() {
        this.health--;
    }
    shoot() {
        console.log('Peeuw, peeuw!');
    }
    move() {
        this.addDistanceTravelled();
        console.log(this.distanceTravelled);
    }
    getName() {
        return this.name;
    }
    getHealth() {
        return this.health;
    }
    getDamage() {
        this.calculateDamage();
    }
}
const spaceShip = new Ship("USS Enterprise", "grijs", 35, 10000, 100);
spaceShip.move();
spaceShip.shoot();
class Asteroid {
    constructor(number, xLocation, yLocation, diameter) {
        this.number = number;
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.diameter = diameter;
    }
    moveDuringFlight() {
        this.xLocation++;
        this.yLocation++;
    }
    fly() {
        this.moveDuringFlight();
        console.log(`Asteroid ${this.number} moved to ${this.xLocation} horizontally and ${this.yLocation} vertically.`);
    }
    touchSpaceShip() {
        console.log(`Just a little scratch, no further damage!`);
    }
    removeAsteroid() {
        console.log(`Asteroid ${this.number} will now be deleted...`);
        delete this.number;
        delete this.diameter;
        delete this.xLocation;
        delete this.yLocation;
    }
    damageSpaceShip() {
        spaceShip.getDamage();
        let spaceShipName = spaceShip.getName();
        let healthOfSpaceShip = spaceShip.getHealth();
        console.log(`That hurts! Now the ${spaceShipName} has ${healthOfSpaceShip} health points left...`);
        let newDiameter = this.diameter / 2;
        this.diameter = newDiameter;
        console.log(`The new diameter of Asteroid ${this.number} is ${this.diameter}`);
        if (this.diameter < 2.5) {
            this.removeAsteroid();
        }
    }
}
const asteroidOne = new Asteroid(1, 60, 32, 12);
const asteroidTwo = new Asteroid(2, 23, 52, 10);
asteroidOne.fly();
asteroidTwo.touchSpaceShip();
asteroidOne.damageSpaceShip();
asteroidOne.damageSpaceShip();
asteroidOne.damageSpaceShip();
//# sourceMappingURL=app.js.map