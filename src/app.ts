class Ship {
    private name: string;
    public color: string;
    private cannons: number;
    private distanceTravelled: number;
    private health: number;
    public constructor(name: string, color: string, cannons: number, distanceTravelled: number, health: number){
        this.name = name;
        this.color = color;
        this.cannons = cannons;
        this.distanceTravelled = distanceTravelled;
        this.health = health;
    }
    private addDistanceTravelled() {
        this.distanceTravelled++;
    }
    private calculateDamage() {
        this.health--;
    }
    shoot() {
        console.log('Peeuw, peeuw!');
    }
    move() {
        this.addDistanceTravelled();
        console.log(this.distanceTravelled)
    }
    getName() {
        return this.name;
    }
    getHealth() {
        return this.health;
    }
    getDamage() {
        this.calculateDamage();
    }
}

const spaceShip: Ship = new Ship("USS Enterprise", "grijs", 35, 10000, 100);
spaceShip.move();
spaceShip.shoot();

class Asteroid {
    // All private, can and may not be changed by the player
    private number: number;
    private xLocation: number;
    private yLocation: number;
    private diameter: number;

    public constructor(number: number, xLocation: number, yLocation: number, diameter: number) {
        this.number = number;
        this.xLocation = xLocation;
        this.yLocation = yLocation;
        this.diameter = diameter;
    }

    private moveDuringFlight() {
        this.xLocation++;
        this.yLocation++;
    }

    fly() {
        this.moveDuringFlight();
        console.log(`Asteroid ${this.number} moved to ${this.xLocation} horizontally and ${this.yLocation} vertically.`)
    }

    touchSpaceShip() {
        console.log(`Just a little scratch, no further damage!`);
    }

    removeAsteroid() {
        console.log(`Asteroid ${this.number} will now be deleted...`)
        delete this.number;
        delete this.diameter;
        delete this.xLocation;
        delete this.yLocation;
    }

    damageSpaceShip() {
        spaceShip.getDamage();
        let spaceShipName = spaceShip.getName();
        let healthOfSpaceShip = spaceShip.getHealth();
        console.log(`That hurts! Now the ${spaceShipName} has ${healthOfSpaceShip} health points left...`);
        let newDiameter = this.diameter / 2;
        this.diameter = newDiameter;
        console.log(`The new diameter of Asteroid ${this.number} is ${this.diameter}`);

        if(this.diameter < 2.5) {
            this.removeAsteroid();
        }
    }

}

    const asteroidOne: Asteroid = new Asteroid(1, 60, 32, 12);
    const asteroidTwo: Asteroid = new Asteroid(2, 23, 52, 10);

    asteroidOne.fly();
    asteroidTwo.touchSpaceShip();
    asteroidOne.damageSpaceShip();
    asteroidOne.damageSpaceShip();
    asteroidOne.damageSpaceShip();
    